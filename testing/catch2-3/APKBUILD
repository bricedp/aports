# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: psykose <alice@ayaya.dev>
pkgname=catch2-3
pkgver=3.2.0
pkgrel=0
arch="all"
url="https://github.com/catchorg/Catch2"
pkgdesc="A modern, C++-native, header-only, test framework for unit-tests (v3)"
license="BSL-1.0"
makedepends="
	cmake
	python3
	samurai
	"
source="https://github.com/catchorg/Catch2/archive/v$pkgver/catch2-v$pkgver.tar.gz"
subpackages="$pkgname-doc"
builddir="$srcdir/Catch2-$pkgver"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=MinSizeRel \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	# ApprovalTests is broken https://github.com/catchorg/Catch2/issues/1780
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest -E "ApprovalTests"
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
983d37824a8d9e24ff107d27f11cb4f8ea53516dc2c5c9b32d4758c718f29041eecdb023d81a2776c87d283e3671722352c9f0eea02393b5bb191fa26bb12c82  catch2-v3.2.0.tar.gz
"
