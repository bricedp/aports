# Maintainer: Hoang Nguyen <folliekazetani@protonmail.com>
pkgname=usql
pkgver=0.13.1
pkgrel=0
pkgdesc="Universal command-line interface for SQL databases"
url="https://github.com/xo/usql"
# riscv64: ftbfs
# x86, armhf, armv7: netezza and cockroachdb drivers fail to build on 32-bit
# - https://github.com/xo/usql/issues/59
# - https://github.com/IBM/nzgo/issues/38
# - https://github.com/cockroachdb/pebble/issues/1575
arch="all !riscv64 !armhf !armv7 !x86"
license="MIT"
makedepends="go unixodbc-dev icu-dev"
source="$pkgname-$pkgver.tar.gz::https://github.com/xo/usql/archive/refs/tags/v$pkgver.tar.gz"

export CGO_ENABLED=1 # needed for godror and odbc drivers
export GOFLAGS="$GOFLAGS -modcacherw"
export GOCACHE="${GOCACHE:-"$srcdir/go-cache"}"
export GOTMPDIR="${GOTMPDIR:-"$srcdir"}"
export GOMODCACHE="${GOMODCACHE:-"$srcdir/go"}"

export _GOTAGS="all sqlite_app_armor sqlite_fts5 sqlite_introspect sqlite_json1 sqlite_stat4 sqlite_userauth sqlite_vtable sqlite_icu"

build() {
	local _goldflags="
	-X github.com/xo/usql/text.CommandName=$pkgname
	-X github.com/xo/usql/text.CommandVersion=$pkgver
	"

	go build -v -ldflags "$_goldflags" -tags "$_GOTAGS" -o $pkgname
}

check() {
	# Tests for specific drivers require docker
	go test $(go list ./... | grep -v /drivers)
}

package() {
	install -Dm755 $pkgname -t "$pkgdir"/usr/bin
}

sha512sums="
3d043178f6eee436a0d2129d357acd98721c04b8c8311af5db834981810956fb34c2ee28c449e9749c98cb4cb6b7a2a2a5896b72ffec28fe7e64bdf79f901aab  usql-0.13.1.tar.gz
"
