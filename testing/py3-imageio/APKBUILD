# Maintainer: Antoni Aloy <aaloytorrens@gmail.com>
pkgname=py3-imageio
pkgver=2.22.4
pkgrel=0
pkgdesc="Python library that provides an easy interface to read and write a wide range of image data"
url="https://github.com/imageio/imageio"
license="BSD-2-Clause"
# ppc64le: test failures
# s390x: freeimage
arch="noarch !ppc64le !s390x"
depends="python3 py3-numpy py3-pillow freeimage"
makedepends="py3-setuptools"
checkdepends="py3-pytest py3-psutil py3-imageio-ffmpeg py3-fsspec"
source="$pkgname-$pkgver.tar.gz::https://files.pythonhosted.org/packages/source/i/imageio/imageio-$pkgver.tar.gz"
builddir="$srcdir/imageio-$pkgver"
options="!check" # intentionally fail without internet(?), todo

build() {
	python3 setup.py build
}

check() {
	PYTHONPATH="$PWD"/build/lib IMAGEIO_NO_INTERNET=1 pytest -v
}

package() {
	python3 setup.py install --root="$pkgdir" --optimize=1

	# remove unneeded binaries
	rm -r "$pkgdir"/usr/bin
}

sha512sums="
0232733d0b5cdba75e0e25878c94b9461c8b33d1a58cecb8d6cdb3dca315c4ba38327f1955290de55779cfabbd9d8a3082187108452bc8ae9971c8dd57037b4d  py3-imageio-2.22.4.tar.gz
"
