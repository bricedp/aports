# Contributor: August Klein <amatcoder@gmail.com>
# Maintainer: André Klitzing <aklitzing@gmail.com>
pkgname=cppcheck
pkgver=2.9.1
pkgrel=0
pkgdesc="Static analysis tool for C/C++ code"
url="https://cppcheck.sourceforge.io/"
# riscv64 broken, "Could not find qmake spec 'default'."
arch="all !riscv64"
license="GPL-3.0-or-later"
makedepends="
	cmake
	docbook-xsl
	pcre-dev
	python3
	qt6-qtcharts-dev
	qt6-qttools-dev
	samurai
	tinyxml2-dev
	"
subpackages="$pkgname-doc $pkgname-htmlreport::noarch $pkgname-gui"
source="$pkgname-$pkgver.tar.gz::https://github.com/danmar/cppcheck/archive/refs/tags/$pkgver.tar.gz
	set_datadir.patch
	python3-htmlreport.patch
	"

build() {
	make DB2MAN=/usr/share/xml/docbook/xsl-stylesheets-*/manpages/docbook.xsl man

	cmake -B build -G Ninja \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_BUILD_TYPE=MinSizeRel \
		-DBUILD_GUI=ON \
		-DBUILD_TESTS="$(want_check && echo ON || echo OFF)" \
		-DFILESDIR=/usr/share/cppcheck \
		-DHAVE_RULES=ON \
		-DUSE_BUNDLED_TINYXML2=OFF \
		-DUSE_MATCHCOMPILER=ON \
		-DUSE_QT6=ON \
		-DWITH_QCHART=ON

	cmake --build build
}

check() {
	ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build
	install -Dm644 cppcheck.1 -t "$pkgdir"/usr/share/man/man1
	install -Dm755 htmlreport/cppcheck-htmlreport -t "$pkgdir"/usr/bin

	mkdir -p "$pkgdir"/usr/share/cppcheck/lang
	mv "$pkgdir"/usr/bin/cppcheck*.qm "$pkgdir"/usr/share/cppcheck/lang
}

htmlreport() {
	pkgdesc="Utility to generate a html report of a XML file produced by cppcheck"
	depends="$pkgname=$pkgver-r$pkgrel python3 py3-pygments"

	amove usr/bin/cppcheck-htmlreport
}

gui() {
	pkgdesc="Qt gui for cppcheck"
	depends="$pkgname=$pkgver-r$pkgrel"

	amove usr/bin/cppcheck-gui
	amove usr/share/cppcheck/lang
	amove usr/share/icons
	amove usr/share/applications
}

sha512sums="
d411874b5dba77a9dd516385b7182d420018c9fb5e9fe1992ceeaa58f39b20df6beed7998ad6e43fd98898f3a8cb82c23f4e7543658ddb2c2d7f4274731d36e2  cppcheck-2.9.1.tar.gz
ebc9cf95f51afe52094f51fa82ae76a6f52b63c5f94df512b61c939328fc0d3e8c681644c61368f3d3267bf6c0a155604cc00b20e49fd196d788a357655a9d43  set_datadir.patch
346c5b41af809dfbff00b7ce66f8abc0e038d6272cf08bbefc4bfc6eaf32940815faca376609165de85072761271521c2a7a56aa7c59e88f65f7d9514b35aaff  python3-htmlreport.patch
"
