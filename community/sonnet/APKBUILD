# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=sonnet
pkgver=5.100.0
pkgrel=0
pkgdesc="Spelling framework for Qt5"
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://community.kde.org/Frameworks"
license="LGPL-2.1-only"
depends="hunspell"
depends_dev="
	qt5-qtbase-dev
	qt5-qtdeclarative-dev
	"
makedepends="$depends_dev
	doxygen
	extra-cmake-modules
	hunspell-dev
	qt5-qttools-dev
	samurai
	"
checkdepends="xvfb-run"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/sonnet-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_QCH=ON
	cmake --build build
}

check() {
	cd build

	# sonnet-test_autodetect fails to detect a speller backend
	CTEST_OUTPUT_ON_FAILURE=TRUE xvfb-run ctest -E "sonnet-test_autodetect"
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
760a95122f41da4ac8842d1869d96d1fd6ae2d92a93e33c89088fee08050b7bd5f9e4668987d47c86556281f97f54d8b13973e18629cde7af13cf1a0b9510cb8  sonnet-5.100.0.tar.xz
"
