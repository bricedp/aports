# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Magnus Sandin <magnus.sandin@gmail.com>
pkgname=py3-pychromecast
pkgver=13.0.1
pkgrel=1
pkgdesc="Python module to talk to Google Chromecast"
url="https://github.com/home-assistant-libs/pychromecast"
arch="noarch"
license="MIT"
depends="
	py3-casttube
	py3-protobuf
	py3-zeroconf
	python3
	"
makedepends="py3-setuptools"
source="https://pypi.python.org/packages/source/P/PyChromecast/PyChromecast-$pkgver.tar.gz"
options="!check" # No tests
builddir="$srcdir/PyChromecast-$pkgver"

provides="py3-chromecast=$pkgver-r$pkgrel" # Backwards compatibility
replaces="py3-chromecast" # Backwards compatibility

build() {
	python3 setup.py build
}

package() {
	python3 setup.py install --prefix=/usr --root="$pkgdir"
}

sha512sums="
86c4119dcd53128c5919627aeb55eb153332826b5b2f8855c59e367098db5201a6f870a8d7a95029393a9522d415ce89e659619608ab11c6cd932b09f694f3a5  PyChromecast-13.0.1.tar.gz
"
