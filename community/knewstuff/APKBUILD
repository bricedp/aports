# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=knewstuff
pkgver=5.100.0
pkgrel=0
pkgdesc="Framework for downloading and sharing additional application data"
# armhf blocked by qt5-qtdeclarative
arch="all !armhf"
url="https://community.kde.org/Frameworks"
license="LGPL-2.1-or-later"
depends="kirigami2"
depends_dev="
	karchive-dev
	kcompletion-dev
	kconfig-dev
	kcoreaddons-dev
	ki18n-dev
	kiconthemes-dev
	kio-dev
	kitemviews-dev
	kpackage-dev
	kservice-dev
	ktextwidgets-dev
	kwidgetsaddons-dev
	kxmlgui-dev
	qt5-qtbase-dev
	qt5-qtdeclarative-dev
	"
makedepends="$depends_dev
	doxygen
	extra-cmake-modules
	qt5-qttools-dev
	samurai
	"
checkdepends="xvfb-run"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/knewstuff-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"

case "$CARCH" in
s390x)
	# times out in knewstuff-installationtest after 300s
	options="$options !check"
	;;
esac

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_QCH=ON
	cmake --build build
}

check() {
	cd build
	XDG_RUNTIME_DIR="$(mktemp -p -d "$builddir")" \
	CTEST_OUTPUT_ON_FAILURE=TRUE xvfb-run -a ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
82ae94a17bef8fca722578ee5326dbcd54b3f2dec619ad6b8b5ccd1e96a64fc407c52b356ef7b72c411104bd3cd801298b73f42e81e3a8489306319f5d00c93e  knewstuff-5.100.0.tar.xz
"
