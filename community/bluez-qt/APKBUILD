# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=bluez-qt
pkgver=5.100.0
pkgrel=0
arch="all !armhf" # armhf blocked by qt5-qtdeclarative
pkgdesc="Qt wrapper for Bluez 5 DBus API"
url="https://community.kde.org/Frameworks"
license="LGPL-2.1-or-later"
depends_dev="qt5-qtbase-dev qt5-qtdeclarative-dev"
makedepends="$depends_dev extra-cmake-modules doxygen graphviz qt5-qttools-dev samurai"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/bluez-qt-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-doc"
options="!check" # Multiple tests either hang or fail completely

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=MinSizeRel \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_QCH=ON
	cmake --build build
}

package() {
	DESTDIR="$pkgdir" cmake --build build --target install
}

sha512sums="
018be9f8d7c20b44f61e12214624fe4516ed71a0bd3868451e22c81dab671f004199d318747b10c5c0b326f94728a797a02bdb3a28939804de8b756f604ca53a  bluez-qt-5.100.0.tar.xz
"
