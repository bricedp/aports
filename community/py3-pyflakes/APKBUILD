# Contributor: Peter Bui <pnutzh4x0r@gmail.com>
# Maintainer: Fabian Affolter <fabian@affolter-engineering.ch>
# NOTE: ensure compatibility with py3-flake8! (#11378)
pkgname=py3-pyflakes
_pkgname=${pkgname#py3-}
pkgver=3.0.0
pkgrel=0
pkgdesc="passive checker of Python programs"
url="https://github.com/PyCQA/pyflakes"
arch="noarch"
license="MIT"
depends="python3"
makedepends="py3-setuptools"
source="https://files.pythonhosted.org/packages/source/p/$_pkgname/$_pkgname-$pkgver.tar.gz"
builddir="$srcdir/$_pkgname-$pkgver"

replaces="py-pyflakes" # Backwards compatibility
provides="py-pyflakes=$pkgver-r$pkgrel" # Backwards compatibility

build() {
	python3 setup.py build
}

check() {
	python3 setup.py test
}

package() {
	mkdir -p "$pkgdir"/usr/bin
	ln -s pyflakes "$pkgdir"/usr/bin/pyflakes-3

	python3 setup.py --quiet install --prefix=/usr --root="$pkgdir"
}

sha512sums="
d05ebafa1d671317d3db5ac5d0a5017459cecfbcee1024628a913ceb070d68bf9fdd060f8fcf12ace586db6c76321be261098f67a789d3304a64fd05103cf86f  pyflakes-3.0.0.tar.gz
"
