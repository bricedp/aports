# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kimageformats
pkgver=5.100.0
pkgrel=0
pkgdesc="Image format plugins for Qt5"
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://community.kde.org/Frameworks"
license="LGPL-2.1-or-later"
makedepends="
	extra-cmake-modules
	karchive-dev
	libavif-dev
	libheif-dev
	libjxl-dev
	libraw-dev
	openexr-dev
	qt5-qtbase-dev
	samurai
	"
checkdepends="xvfb-run"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/kimageformats-$pkgver.tar.xz"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DKIMAGEFORMATS_HEIF=ON
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE xvfb-run ctest -E "kimageformats-read-psd"
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
6c12e255f1e4373d2dde63e48381c8dfa10a21070efa4efaab53abb11ca0cdd92bb0606c3d6189e0b00e25be1d586d723de2c5d70e0d2cc8ca99aa0c53dc2229  kimageformats-5.100.0.tar.xz
"
