# Contributor: Kevin Daudt <kdaudt@alpinelinux.org>
# Maintainer: Kevin Daudt <kdaudt@alpinelinux.org>
pkgname=py3-stack_data
pkgver=0.6.1
pkgrel=1
pkgdesc="library that extracts data from stack frames and tracebacks"
url="https://github.com/alexmojaki/stack_data"
arch="noarch"
license="MIT"
depends="python3 py3-executing py3-asttokens py3-pure_eval"
makedepends="py3-gpep517 py3-setuptools_scm py3-wheel"
checkdepends="py3-pytest py3-typeguard"
options="!check" # requires unpackaged littleutils
source="$pkgname-$pkgver.tar.gz::https://github.com/alexmojaki/stack_data/archive/refs/tags/v$pkgver.tar.gz"
builddir="$srcdir/stack_data-$pkgver"

build() {
	export SETUPTOOLS_SCM_PRETEND_VERSION=$pkgver
	gpep517 build-wheel \
		--wheel-dir dist \
		--output-fd 1
}

check() {
	pytest
}

package() {
	local whl=dist/stack_data-$pkgver-py3-none-any.whl
	python3 -m installer --dest="$pkgdir" "$whl"
}

sha512sums="
c8e7a5c26c4d76fce69ec2cbb9326f3e1837a904cedc4fe193682f4c635c974ef1825ac4ac57d798b6af1dc4de3c27abed7165c452ab418799a203477449bed8  py3-stack_data-0.6.1.tar.gz
"
