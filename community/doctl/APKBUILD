# Contributor: Will Sinatra <wpsinatra@gmail.com>
# Maintainer: Will Sinatra <wpsinatra@gmail.com>
pkgname=doctl
pkgver=1.88.0
pkgrel=0
pkgdesc="Official command line interface for the DigitalOcean API"
url="https://github.com/digitalocean/doctl"
license="Apache-2.0"
arch="all"
makedepends="go"
subpackages="
	$pkgname-bash-completion
	$pkgname-zsh-completion
	$pkgname-fish-completion
	"
source="https://github.com/digitalocean/doctl/archive/v$pkgver/doctl-$pkgver.tar.gz"

export GOCACHE="${GOCACHE:-"$srcdir/go-cache"}"
export GOTMPDIR="${GOTMPDIR:-"$srcdir"}"
export GOMODCACHE="${GOMODCACHE:-"$srcdir/go"}"

build() {
	maj_min=${pkgver%*.*}
	major=${maj_min%.*}
	minor=${maj_min#*.}
	patch=${pkgver#*.*.*}

	go build \
		-trimpath \
		-buildmode=pie \
		-mod=readonly \
		-ldflags "-extldflags \"$LDFLAGS\" \
			-X github.com/digitalocean/doctl.Major=$major \
			-X github.com/digitalocean/doctl.Minor=$minor \
			-X github.com/digitalocean/doctl.Patch=$patch \
			-X github.com/digitalocean/doctl.Label=alpine-$pkgrel" \
		./cmd/...
}

check() {
	go test -v -mod=readonly ./integration
}

package() {
	install -Dm755 doctl -t "$pkgdir"/usr/bin

	# setup completions
	mkdir -p "$pkgdir"/usr/share/bash-completion/completions \
		"$pkgdir"/usr/share/zsh/site-functions \
		"$pkgdir"/usr/share/fish/completions

	"$pkgdir"/usr/bin/doctl completion bash > "$pkgdir"/usr/share/bash-completion/completions/doctl
	"$pkgdir"/usr/bin/doctl completion zsh > "$pkgdir"/usr/share/zsh/site-functions/_doctl
	"$pkgdir"/usr/bin/doctl completion fish > "$pkgdir"/usr/share/fish/completions/doctl.fish
}

sha512sums="
629bcc806c9270b4f0e70f6959316c6a7a3195e9d8105243be7a9a3e311b01c01f9389fbb93cd8c9cd1c23451d98fe5802e106b19378e427e941cc817218e589  doctl-1.88.0.tar.gz
"
