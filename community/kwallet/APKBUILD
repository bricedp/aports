# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kwallet
pkgver=5.100.0
pkgrel=0
pkgdesc="Secure and unified container for user passwords"
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://community.kde.org/Frameworks"
license="LGPL-2.1-or-later"
depends_dev="
	gpgme-dev
	kconfig-dev
	kconfigwidgets-dev
	kcoreaddons-dev
	kdbusaddons-dev
	ki18n-dev
	kiconthemes-dev
	knotifications-dev
	kservice-dev
	kwindowsystem-dev
	libgcrypt-dev
	qca-dev
	"
makedepends="$depends_dev
	doxygen
	extra-cmake-modules
	graphviz
	kdoctools-dev
	qt5-qttools-dev
	samurai
	"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/kwallet-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"
provides="dbus:org.freedesktop.Secrets"
# 2nd highst priority after gnome-keyring, mainly for KDE
provider_priority=10

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_QCH=ON
	cmake --build build
}

check() {
	cd build

	local excluded_checks=
	case "$CARCH" in
		s390x) excluded_checks="(blowfishtest)";;
	esac
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest -E "$excluded_checks"
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
01f5bdbcf2f8909d0dfc044f159847f736d479d720a5f5cd6f69b578b0225d2855a7461eb6e2ec7bc26cf4c7904165db75ba360f6b0ad0e163439fc767dfddeb  kwallet-5.100.0.tar.xz
"
