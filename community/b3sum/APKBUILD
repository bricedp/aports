# Contributor: Leo <thinkabit.ukim@gmail.com>
# Maintainer: psykose <alice@ayaya.dev>
pkgname=b3sum
pkgver=1.3.2
pkgrel=0
pkgdesc="Command line implementation of the BLAKE3 hash function"
url="https://blake3.io"
arch="all !s390x !riscv64"
license="CC0-1.0 OR Apache-2.0"
makedepends="cargo"
source="b3sum-$pkgver.tar.gz::https://crates.io/api/v1/crates/b3sum/$pkgver/download"

case "$CARCH" in
armhf)
	# hang forever, probably due to non-native hardware
	options="$options !check"
	;;
aarch64|armv7)
	_features="neon"
	;;
esac

prepare() {
	default_prepare

	cargo fetch --locked
}

build() {
	cargo build --release --frozen ${_features:+--features $_features}
}

check() {
	cargo test --frozen ${_features:+--features $_features}
}

package() {
	install -Dm755 target/release/b3sum -t "$pkgdir"/usr/bin
}

sha512sums="
971d825f8e00ef90d65b6b33053c002055148f99569ada6e0ef37eac51fc6e7a5a33f81b8e71b432df6c00a0ca3bdc0bd60016822fa93e12368d00ffb7da12c2  b3sum-1.3.2.tar.gz
"
