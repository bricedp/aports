# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kdbusaddons
pkgver=5.100.0
pkgrel=0
pkgdesc="Addons to QtDBus"
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://community.kde.org/Frameworks"
license="LGPL-2.1-only OR LGPL-3.0-only"
makedepends="
	doxygen
	extra-cmake-modules
	graphviz
	qt5-qttools-dev
	shared-mime-info
	samurai
	"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/kdbusaddons-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"
options="!check" # Requires running dbus-daemon

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_QCH=ON
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
196e1ce740af77ef8ab57831ba1a0e086822107436bf50e0e6cef209dbe36f29788139f8b17799441a7a07086af324248d0e626027f9fa5810ba745c22daab2f  kdbusaddons-5.100.0.tar.xz
"
