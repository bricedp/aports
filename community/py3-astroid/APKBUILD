# Contributor: Fabian Affolter <fabian@affolter-engineering.ch>
# Maintainer: Fabian Affolter <fabian@affolter-engineering.ch>
pkgname=py3-astroid
pkgver=2.12.13
pkgrel=1
pkgdesc="A new abstract syntax tree from Python's ast"
url="https://pylint.pycqa.org/projects/astroid/en/latest/"
arch="noarch"
license="LGPL-2.1-or-later"
depends="python3 py3-lazy-object-proxy py3-wrapt"
replaces="py-logilab-astng"
makedepends="
	py3-gpep517
	py3-installer
	py3-setuptools
	py3-wheel
	"
checkdepends="py3-pytest py3-typing-extensions"
source="py3-astroid-$pkgver.tar.gz::https://github.com/PyCQA/astroid/archive/refs/tags/v$pkgver.tar.gz"
builddir="$srcdir/astroid-$pkgver"

build() {
	gpep517 build-wheel \
		--wheel-dir dist \
		--output-fd 1
}

check() {
	pytest
}

package() {
	python3 -m installer -d "$pkgdir" \
		dist/astroid-*.whl
}

sha512sums="
16d062b1a619663b157c4753f1744e56e7d0ef7cace12660feb7f6bc540e54be02638b52c1c877d3eb514e2d7423c4afc71e7d5464880bfd3724ff7ae6d7b90a  py3-astroid-2.12.13.tar.gz
"
